package common

import (
	"fmt"
	"stack-overflow/models"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

type Database struct {
	*gorm.DB
}

var DB *gorm.DB

const NBSecretPassword = "A String Very Very Very Strong!!@##$!@#$"
const NBRandomPassword = "A String Very Very Very Niubilty!!@##$!@#4"

// Opening a database and save the reference to `Database` struct.
func Init() *gorm.DB {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=stackoverflow password=postgres")
	if err != nil {
		fmt.Println("db err: ", err)
	}
	db.LogMode(true)
	db.DB().SetMaxIdleConns(10)
	// db.DropTable(&models.User{}, &models.Answer{}, &models.Question{}, &models.Tag{})
	db.AutoMigrate(&models.User{}, &models.Answer{}, &models.Question{}, &models.Tag{})
	//db.LogMode(true)
	DB = db
	return DB
}
func GetDB() *gorm.DB {
	return DB
}

func GenToken(id uint) string {
	jwt_token := jwt.New(jwt.GetSigningMethod("HS256"))
	// Set some claims
	jwt_token.Claims = jwt.MapClaims{
		"id":  id,
		"exp": time.Now().Add(time.Hour * 24).Unix(),
	}
	// Sign and get the complete encoded token as a string
	token, _ := jwt_token.SignedString([]byte(NBSecretPassword))
	return token
}
