package serializer

import (
	"fmt"
	"stack-overflow/common"
	"stack-overflow/models"
)

type LoginSignupSerializer struct {
	User models.User
}

type UserResponse struct {
	Username string `json:"username"`
	Mail     string `json:"mail"`
	Token    string `json:"token"`
}

func (self LoginSignupSerializer) Response() UserResponse {
	myUserModel := self.User
	user := UserResponse{
		Username: myUserModel.Username,
		Mail:     myUserModel.Mail,
		Token:    common.GenToken(myUserModel.ID),
	}
	return user
}

type TagSerializer struct {
	Tag models.Tag
}
type TagResponseWithQuestion struct {
	Name      string             `json: "name"`
	Questions []QuestionResponse `json: "questions"`
}
type TagResponse struct {
	Name string `json: "name"`
}
type AnswerSerializer struct {
	Answer models.Answer
}
type AnswerResponse struct {
	QuestionId uint   `json: question_id`
	Body       string `json: "body"`
	UserId     uint   `json: user_id`
	Username   string `json:"username"`
	Mail       string `json:"mail"`
}
type QuestionSerializer struct {
	Question models.Question
}

type QuestionResponse struct {
	QuestionId  uint             `json: question_id`
	UserId      uint             `json: "user_id"`
	Title       string           `json: "title"`
	Description string           `json: "description"`
	Tags        []TagResponse    `json: "tags"` // this should be changed to TagResponse
	Answers     []AnswerResponse `json: "answers"`
}

func (self QuestionSerializer) Response() QuestionResponse {
	db := common.GetDB()
	QuestionModel := self.Question
	db.Preload("Tags").First(&QuestionModel)
	db.Preload("Answers").First(&QuestionModel)
	var tags []TagResponse
	for _, tag := range QuestionModel.Tags {
		newTag := TagResponse{Name: tag.Name}
		tags = append(tags, newTag)
	}
	var answers []AnswerResponse
	for _, ans := range QuestionModel.Answers {
		newAns := AnswerSerializer{Answer: ans}.Response()
		// var user models.User
		// fmt.Printf("To check the value of ans: %+v \n", ans)
		// db.Model(ans).Related(&user)
		// fmt.Printf("in question Response %+v \n", user)
		// newAns := AnswerResponse{
		// 	QuestionId: ans.Question,
		// 	Body:       ans.Body,
		// 	UserId:     user.ID,
		// 	Username:   user.Username,
		// 	Mail:       user.Mail,
		// }
		answers = append(answers, newAns)
	}
	question := QuestionResponse{
		QuestionId:  QuestionModel.ID,
		UserId:      QuestionModel.User,
		Title:       QuestionModel.Title,
		Description: QuestionModel.Description,
		Tags:        tags,
		Answers:     answers,
	}
	return question
}

func (self TagSerializer) Response() TagResponseWithQuestion {
	TagModel := self.Tag
	db := common.GetDB()
	db.Preload("Questions").First(&TagModel)

	fmt.Printf("In Serializer package TagModel: %+v \n", TagModel)
	var questions []QuestionResponse
	for _, question := range TagModel.Questions {
		newQuestion := QuestionSerializer{Question: *question}.Response()
		questions = append(questions, newQuestion)
	}
	fmt.Printf("In Serializer package %+v \n", questions)
	tag := TagResponseWithQuestion{
		Name:      TagModel.Name,
		Questions: questions,
	}
	return tag
}

func (self AnswerSerializer) Response() AnswerResponse {
	ans := self.Answer
	db := common.GetDB()
	var user models.User
	db.First(&user, ans.User)
	fmt.Printf("in question Response %+v \n", user)
	answerResponse := AnswerResponse{
		QuestionId: ans.Question,
		Body:       ans.Body,
		UserId:     user.ID,
		Username:   user.Username,
		Mail:       user.Mail,
	}
	return answerResponse
}
