package usercrud

import (
	"fmt"
	"stack-overflow/models"

	"github.com/jinzhu/gorm"
)

func ExistById(db *gorm.DB, id int) (models.User, bool) {
	var user models.User
	notFound := db.First(&user, id).RecordNotFound()
	fmt.Printf("in usercrud: %+v", user)
	return user, !notFound
}
