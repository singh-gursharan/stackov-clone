package answercrud

import (
	"stack-overflow/binders"
	"stack-overflow/common"
	"stack-overflow/models"
)

func Add(userId uint, q binders.Answer) (models.Answer, error) {
	db := common.GetDB()
	answer := models.Answer{Body: q.Body, Question: q.Question, User: userId}
	err := db.Create(&answer).Error
	return answer, err
}
