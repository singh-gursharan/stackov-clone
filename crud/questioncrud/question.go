package questioncrud

import (
	"fmt"
	"stack-overflow/binders"
	"stack-overflow/common"
	"stack-overflow/models"
)

func Create(userId uint, q binders.Question) (models.Question, error) {
	db := common.GetDB()
	var tags []*models.Tag
	for _, tagjson := range q.Tags {
		newTag := &models.Tag{Name: tagjson.Name}
		tags = append(tags, newTag)
	}
	question := models.Question{Title: q.Title, Description: q.Description, Tags: tags, User: userId}
	err := db.Create(&question).Error
	fmt.Printf("question create in questioncrud: %+v \n", question)
	return question, err
}

func Update(q binders.UpdateSubscription) (models.Question, error) {
	db := common.GetDB()
	question, err1 := QuestionById(q.QuestionId)
	if err1 != nil {
		return question, err1
	}
	err2 := db.Model(&question).UpdateColumns(models.Question{Description: q.Description}).Error
	fmt.Printf("question update in questioncrud: %+v \n", question)
	return question, err2
}

func QuestionById(id uint) (models.Question, error) {
	var question models.Question
	db := common.GetDB()
	fmt.Printf("question id for updation: %d \n", id)
	err := db.First(&question, id).Error
	fmt.Printf("in usercrud: %+v \n", question)
	return question, err
}
