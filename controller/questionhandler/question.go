package questionhandler

import (
	"fmt"
	"net/http"
	"stack-overflow/binders"
	"stack-overflow/crud/questioncrud"
	"stack-overflow/serializer"

	"github.com/gin-gonic/gin"
)

// type Tag struct {
// 	Name string `json: "name"`
// }
// type Question struct {
// 	Title       string `json:"title" binding:"required"`
// 	Description string `json:"description"`
// 	Tags        []Tag  `json: "tags" binding: "required"`
// }

func Create() gin.HandlerFunc {
	return func(c *gin.Context) {
		// cookie, err := c.Cookie("login")
		// if err != nil {
		// 	c.Redirect(http.StatusGone, "/login")
		// 	return
		// }
		// user, err := auth.Authorize(cookie)
		var jsonPost binders.Question
		if err := c.ShouldBindJSON(&jsonPost); err != nil {
			c.JSONP(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		userId := c.MustGet("my_user_id").(uint)
		question, err := questioncrud.Create(userId, jsonPost)
		if err != nil {
			c.JSONP(http.StatusAlreadyReported, gin.H{
				"error": err.Error(),
			})
			return
		}
		serial := serializer.QuestionSerializer{Question: question}
		c.JSON(http.StatusOK, gin.H{
			"question": serial.Response(),
		})
	}
}

func UpdateDescription() gin.HandlerFunc {
	return func(c *gin.Context) {
		var updateDesc binders.UpdateSubscription
		if err := c.ShouldBindJSON(&updateDesc); err != nil {
			c.JSONP(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		fmt.Printf("In update description handler %+v\n", updateDesc)
		// userId := c.MustGet("my_user_id").(uint)
		question, err := questioncrud.Update(updateDesc)
		if err != nil {
			c.JSONP(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		serial := serializer.QuestionSerializer{Question: question}
		c.JSONP(http.StatusOK, gin.H{
			"question": serial.Response(),
		})
	}
}
