package userhandler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Create() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"create": "success",
		})
	}
}
