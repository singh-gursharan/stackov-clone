package main

import (
	"fmt"
	"net/http"
	"stack-overflow/common"
	"stack-overflow/controller/answerhandler"
	"stack-overflow/controller/questionhandler"
	"stack-overflow/controller/taghandler"
	"stack-overflow/middleware"
	"stack-overflow/models"
	"stack-overflow/serializer"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//models
// type User struct {
// 	gorm.Model
// 	Username string `form:"username" binding:"required"`
// 	Mail     string `gorm:"unique_index;not null" form:"mail" binding:"required"`
// 	Password string `form:"password" binding:"required"`
// }
type LoginForm struct {
	Mail     string `form:"mail" binding:"required"`
	Password string `form:"password" binding:"required"`
}

type LoginError struct {
	err string
}

// type User struct {
// 	User string `form:"user"`
// 	Mail string `form:"mail"`
// 	Password string `form:"password"`
// }
func main() {
	// db, _ := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=stackoverflow password=postgres")
	db := common.Init()
	defer db.Close()
	db.LogMode(true)
	fmt.Println("hello")
	r := gin.Default()

	// r.POST("/signup", userhandler.Create())
	r.POST("/signup", func(c *gin.Context) {
		var form models.User
		if err := c.ShouldBind(&form); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		fmt.Printf("form: %+v \n", form)
		if Userexist(db, form) {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "already exists"})
			return
		}
		user := Create(db, form)
		c.SetCookie("login", string(user.ID), 100000, "/", "localhost", false, true)
		serial := serializer.LoginSignupSerializer{user}
		c.JSON(http.StatusOK, gin.H{
			"user": serial.Response(),
		})
	})
	r.POST("/login", func(c *gin.Context) {
		var form LoginForm
		if err := c.ShouldBind(&form); err != nil {
			c.JSONP(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		user, err := Login(db, form)
		if err != nil {
			c.JSONP(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		}
		fmt.Printf("user after login: %+v\n", user)
		c.SetCookie("login", string(user.ID), 100000, "/", "localhost", false, true)
		serial := serializer.LoginSignupSerializer{user}
		c.JSONP(http.StatusOK, gin.H{"user": serial.Response()})
	})
	v1 := r.Group("/api")
	v1.Use(middleware.AuthMiddleware())
	{
		v1.POST("/questions", questionhandler.Create())
		v1.PUT("/questions", questionhandler.UpdateDescription())
		v1.POST("/questions/:question_id/answers", answerhandler.Create())
	}
	r.GET("/tags", taghandler.GetAllQuestionByTag())

	r.Run()

}

func (e LoginError) Error() string {
	return e.err
}

func Login(db *gorm.DB, form LoginForm) (models.User, error) {
	user := models.User{}
	NotFound := db.Where("mail = ? AND password = ?", form.Mail, form.Password).First(&user).RecordNotFound()
	fmt.Printf("user: %+v \n", user)
	if NotFound {
		err := LoginError{err: "no such user exist"}
		return user, err
	}
	return user, nil

}
func Userexist(db *gorm.DB, form models.User) bool {
	var user models.User
	err := db.Where("mail =?", form.Mail).First(&user).RecordNotFound()
	fmt.Printf("user: %+v \n", user)
	return !err
}
func Create(db *gorm.DB, form models.User) models.User {
	db.Create(&form)
	fmt.Printf("create returns: %+v \n", form)
	return form
}
