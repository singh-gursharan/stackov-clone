package answerhandler

import (
	"fmt"
	"net/http"
	"stack-overflow/binders"
	"stack-overflow/crud/answercrud"
	"stack-overflow/serializer"

	"github.com/gin-gonic/gin"
)

func Create() gin.HandlerFunc {
	return func(c *gin.Context) {
		var jsonPost binders.Answer
		if err := c.ShouldBindJSON(&jsonPost); err != nil {
			c.JSONP(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
		fmt.Printf("In create of AnswerHandler %+v \n", jsonPost)
		userId := c.MustGet("my_user_id").(uint)
		answer, err := answercrud.Add(userId, jsonPost)
		if err != nil {
			c.JSONP(http.StatusAlreadyReported, gin.H{
				"error": err.Error(),
			})
			return
		}
		serial := serializer.AnswerSerializer{Answer: answer}
		c.JSONP(http.StatusCreated, gin.H{
			"answer": serial.Response(),
		})
	}
}
