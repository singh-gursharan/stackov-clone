package taghandler

import (
	"fmt"
	"net/http"
	"stack-overflow/crud/tagcrud"
	"stack-overflow/serializer"

	"github.com/gin-gonic/gin"
)

func GetAllQuestionByTag() gin.HandlerFunc {
	return func(c *gin.Context) {
		tag := c.Query("tag")
		fmt.Printf("In tagHandler GetAllQuestionByTag: %s \n", tag)
		tagModel, err := tagcrud.GetByName(tag)
		if err != nil {
			c.JSONP(http.StatusNotFound, gin.H{
				"error": err.Error(),
			})
			return
		}
		serial := serializer.TagSerializer{Tag: tagModel}
		c.JSONP(http.StatusOK, gin.H{
			"tag": serial.Response(),
		})
	}
}
