package binders

type Tag struct {
	Name string `json:"name"`
}
type Question struct {
	Title       string `json:"title" binding:"required"`
	Description string `json:"description"`
	Tags        []Tag  `json:"tags" binding:"required"`
}

type UpdateSubscription struct {
	QuestionId  uint   `json:"question_id" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type Answer struct {
	Body     string `json:"body"`
	Question uint   `json:"question_id"`
}
