package middleware

import (
	"fmt"
	"net/http"
	"stack-overflow/common"
	"stack-overflow/crud/usercrud"
	"stack-overflow/models"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Autherror struct {
	err string
}

func (e Autherror) Error() string {
	return e.err
}
func stripBearerPrefixFromTokenString(tok string) (string, error) {
	// Should be a bearer token
	if len(tok) > 5 && strings.ToUpper(tok[0:6]) == "TOKEN " {
		return tok[6:], nil
	}
	return tok, nil
}
func Authorize(db *gorm.DB, cookie string) (models.User, error) {
	fmt.Printf("cookie in auth: %s\n", cookie)
	id := 9
	user, found := usercrud.ExistById(db, id)
	if !found {
		err := Autherror{err: "cookie not authenticated, no such user exist"}
		return user, err
	}
	return user, nil
}

var AuthorizationHeaderExtractor = &request.PostExtractionFilter{
	request.HeaderExtractor{"Authorization"},
	stripBearerPrefixFromTokenString,
}

var MyAuth2Extractor = &request.MultiExtractor{
	AuthorizationHeaderExtractor,
	request.ArgumentExtractor{"access_token"},
}

func UpdateContextUserModel(c *gin.Context, my_user_id uint) {
	var myUserModel models.User
	if my_user_id != 0 {
		db := common.GetDB()
		db.First(&myUserModel, my_user_id)
	}
	c.Set("my_user_id", my_user_id)
	c.Set("my_user_model", myUserModel)
}
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		UpdateContextUserModel(c, 0)
		token, err := request.ParseFromRequest(c.Request, MyAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
			b := ([]byte(common.NBSecretPassword))
			return b, nil
		})
		if err != nil {
			c.AbortWithError(http.StatusUnauthorized, err)
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			my_user_id := uint(claims["id"].(float64))
			fmt.Println("in autherization middleware:  ", my_user_id, claims["id"])
			UpdateContextUserModel(c, my_user_id)
		}
	}
}
