package models

import (
	"github.com/jinzhu/gorm"
)

type Question struct {
	gorm.Model
	User        uint
	Title       string `gorm:"not null"`
	Description string
	Answers     []Answer `gorm:"foreignkey:Question"`
	Tags        []*Tag   `gorm:"many2many:question_tags"`
}
