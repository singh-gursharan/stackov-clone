package models

import "github.com/jinzhu/gorm"

type Answer struct {
	gorm.Model
	Body     string `gorm:"not null"`
	Question uint
	User     uint
}
