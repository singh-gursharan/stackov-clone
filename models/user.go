package models

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	Username  string     `form:"username" binding:"required"`
	Mail      string     `gorm:"unique_index;not null" form:"mail" binding:"required"`
	Password  string     `form:"password" binding:"required"`
	Qusetions []Question `gorm:"foreignkey:User"`
	Answers   []Answer   `gorm:"foreignkey:User"`
}
