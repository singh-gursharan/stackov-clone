package models

import "time"

type Tag struct {
	// gorm.Model
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
	Name      string      `gorm:"primary_key"`
	Questions []*Question `gorm:"many2many:question_tags;not null"`
}
